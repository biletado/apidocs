# Run the http-client within podman

## local execution

### build the validators first 
You need either the v2 or the v3 validators.
This will generate `validate-*.js` files in the subdirectories of `public` which are necessary to test the responses
against the given OpenAPI-Schema.
See [HTTP Client Schema check](https://gitlab.com/http-client-schema-check/http-client-schema-check/) for more details.

#### v2
```shell
podman run --rm -i -t \
    -e DIRECTORY=/app/openapi/v2 \
    --security-opt="label=disable" \
    -v $(pwd)/public:/app/openapi \
    registry.gitlab.com/http-client-schema-check/http-client-schema-check:latest
```

#### v3
```shell
podman run --rm -i -t \
    -e DIRECTORY=/app/openapi/v3 \
    --security-opt="label=disable" \
    -v $(pwd)/public:/app/openapi \
    registry.gitlab.com/http-client-schema-check/http-client-schema-check:latest
```
### run within IntelliJ

> This will not work with IntelliJ Community because it doesn't have support for the HTTP Client.

Open the http-files and select the environment `kind` before executing.
You need to execute the test `000_Authenticate.http` before any write test suite
to have a valid token and authentication cookie.

### alternative: run in a container
You can also use the intellij-http-client container image to run the tests which is free to use and doesn't need a 

```shell
podman run --rm -i -t \
    -v $PWD:/workdir \
    --security-opt="label=disable" \
    --add-host host.docker.internal:host-gateway \
    docker.io/jetbrains/intellij-http-client \
        tests/000_Authenticate.http tests/100_JwtPong.http tests/200_ReadAssets-v3.http tests/300_WriteAssets-v3.http \
        --env-file http-client.env.json \
        --env kind \
        -D \
        --report
```

> You may also need `--network slirp4netns:allow_host_loopback=true` when running in podman <5

## prebuild image
This project creates a container-image with prebuild validators which can be executed directly and without the need
to generate the validators.

```shell
podman run --rm -i -t \
    --security-opt="label=disable" \
    -v $(pwd)/reports:/workdir/reports \
    --add-host host.docker.internal:host-gateway \
    registry.gitlab.com/biletado/apidocs \
        tests/000_Authenticate.http tests/100_JwtPong.http tests/200_ReadAssets-v3.http tests/300_WriteAssets-v3.http \
        --env-file /workdir/http-client.env.json \
        --env kind \
        -D \
        --report
```

### with local adjustments in the tests

```shell
podman run --rm -i -t \
    --security-opt="label=disable" \
    -v $(pwd)/reports:/workdir/reports \
    -v $(pwd)/tests:/workdir/tests \
    -v $(pwd)/http-client.env.json:/workdir/http-client.env.json \
    --add-host host.docker.internal:host-gateway \
    registry.gitlab.com/biletado/apidocs \
        tests/000_Authenticate.http tests/100_JwtPong.http tests/200_ReadAssets-v3.http tests/300_WriteAssets-v3.http \
        --env-file /workdir/http-client.env.json \
        --env kind \
        -D \
        --report
```
